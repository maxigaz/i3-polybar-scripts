#!/usr/bin/env bash

function weather() {
	DATA=$(curl -s https://www.idokep.hu/idojaras/Debrecen)

	# Current temperature and weather condition
	NOW_DATA=$(grep -A 33 "shortCurrentWeatherText" <<< $DATA)

	NOW_TEMP=$(grep -m 1 'current-temperature' <<< $NOW_DATA | grep -o "\-\?[0-9][0-9]\?˚C" | sed 's/˚/°/')
	NOW_COND_RAW=$(grep -A 2 'current-weather-icon' <<< $NOW_DATA | grep -o "[0-9][0-9][0-9].svg" | sed 's/\.svg//')

	# High/low temperature and overall weather condition predicted for today/tomorrow
	FC_DATA=$(grep -m 1 -A 50 'dfIconAlert' <<< $DATA)

	HIGH_TEMP=$(grep -m 1 'max-daily-temp' <<< $FC_DATA | grep -o "\-\?[0-9][0-9]\?°C")
	LOW_TEMP=$(grep 'max-daily-temp' <<< $FC_DATA | tail -n1 | grep -o "\-\?[0-9][0-9]\?°C")
	FC_COND_RAW=$(grep -m 1 -o "[0-9][0-9][0-9].svg" <<< $FC_DATA | sed 's/\.svg//')


	function geticon() {
		case "$CODE" in
			"010")
				CONDITION="derült"
				ICON=""
				;;
			"310")
				CONDITION="derült"
				ICON=""
				;;
			"021")
				CONDITION="gyengén felhős"
				ICON=""
				;;
			"321")
				CONDITION="gyengén felhős"
				ICON=""
				;;
			"022")
				CONDITION="közepesen felhős"
				ICON=""
				;;
			"322")
				CONDITION="közepesen felhős"
				ICON=""
				;;
			"323"|"023")
				CONDITION="erősen felhős"
				ICON=""
				;;
			"330"|"030")
				CONDITION="borult"
				ICON=""
				;;
			"041")
				CONDITION="szitálás"
				ICON=""
				;;
			"341")
				CONDITION="szitálás"
				ICON=""
				;;
			"342"|"042")
				CONDITION="gyenge eső"
				ICON=""
				;;
			"343"|"043")
				CONDITION="eső"
				ICON=""
				;;
			"343s")
				CONDITION="eső széllel"
				ICON=""
				;;
			"351"|"051")
				CONDITION="havas eső"
				ICON=""
				;;
			"352"|"052")
				CONDITION="ónos eső"
				ICON=""
				;;
			"061")
				CONDITION="hószállingózás"
				ICON=""
				;;
			"361")
				CONDITION="hószállingózás"
				ICON=""
				;;
			"362"|"062")
				CONDITION="havazás"
				ICON=""
				;;
			"363"|"063")
				CONDITION="intenzív havazás"
				ICON=""
				;;
			"381"|"081")
				CONDITION="zápor"
				ICON=""
				;;
			"083")
				CONDITION="hózápor"
				ICON=""
				;;
			"383")
				CONDITION="hózápor"
				ICON=""
				;;
			"390"|"090")
				CONDITION="zivatar"
				ICON=""
				;;
			"092")
				CONDITION="száraz zivatar"
				ICON=""
				;;
			"011")
				CONDITION="pára"
				ICON=""
				;;
			"311")
				CONDITION="pára"
				ICON=""
				;;
			"100")
				CONDITION="köd"
				ICON=""
				;;
		esac
	}

	CODE=$NOW_COND_RAW
	geticon
	NOW_ICON=$ICON

	CODE=$FC_COND_RAW
	geticon
	FC_ICON=$ICON

	# Print it!
	echo -e "%{T5}$NOW_ICON%{T-} $NOW_TEMP | %{T5}$FC_ICON%{T-} %{F#3498db}$LOW_TEMP %{F#fa4c3c}$HIGH_TEMP%{F-}"
}

trap "weather" USR1

while [[ true ]]; do
	weather
	sleep 1800
done
