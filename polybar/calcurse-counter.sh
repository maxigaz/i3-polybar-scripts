#!/usr/bin/env bash
# Show the number of appointments recorded for today and tomorrow
echo $(calcurse -d 2 | grep -c "^ \*\|^ -")
