#!/usr/bin/env bash

awk "!/^x/ && /due:$(date +"%Y-%m-%d")/" "path/to/todo.txt" | wc -l
