# Scripts for Polybar

Note: With the exception of the weather script, having [Nerd Fonts](https://www.nerdfonts.com/) or Font Awesome installed is recommended in order for icons to appear correctly.

## `calcurse-counter.sh`

It shows the number of appointments recorded for today and tomorrow in [calcurse](https://github.com/lfos/calcurse).

## `idokep-high-low.sh`

It parses idokep.hu (Hungarian weather forecast) and shows the following for Debrecen (edit the script if you want info for another town):

- Icon for the current weather condition
- Current temperature
- Icon for the weather condition predicted for today or the next day
- The lowest and highest predicted temperatures for today or the next day

Left click on the module to update the script.

Screenshot:

![idokep script screenshot](img/idokep.png)

Dependencies:

- sed
- grep
- [Weather Icons](https://erikflowers.github.io/weather-icons/) correctly set up in Polybar

Polybar config example:

```
[module/idokep]
type = custom/script

exec = ~/.config/polybar/scripts/idokep-high-low.sh

tail = true
click-left = kill -USR1 %pid%
```

## `ds4-battery`

Shows the current battery status of a DualShock 4 gamepad. Inspired by [this script](https://github.com/polybar/polybar-scripts/tree/master/polybar-scripts/info-dualshock4).

This is an odd one out: I don’t use an external script file for this, I just have the following added to my Polybar config:

```
[module/ds4-battery]
type = custom/script
exec = echo "  $(cat /sys/class/power_supply/sony_controller_battery_*/capacity)%"
exec-if = [ -d /sys/class/power_supply/sony_controller_battery_* ]
interval = 10

format = <label>

click-left = bluetoothctl -- disconnect; notify-send "DualShock 4" "Disconnected"
```

## `todo.txt-polybar.sh`

Shows the number of entries due today found in your [todo.txt](https://github.com/todotxt/todo.txt). It excludes the ones marked as completed (their lines start with `x`).

Screenshot:

![todo.txt script screenshot](img/todo.txt.png)

Dependencies:

- awk
- date
- wc

Polybar config example:

```
[module/todo-txt]
interval = 3600
type = custom/script
exec = ~/.config/polybar/scripts/todo.txt-polybar.sh
exec-if = [ $(~/.config/polybar/scripts/todo.txt-polybar.sh) -gt 0 ]
format = <label>
format-prefix = " "
click-left = alacritty -e nvim "path/to/todo.txt"
```

