#!/usr/bin/env bash
if [[ $(pgrep -u $UID -x xdotool) ]]; then
	pkill xdotool
	dunstify -r 9 -t 2000 "autoclick" "disabled"
else
	dunstify -r 9 -t 2000 "autoclick" "enabled"
	xdotool click --repeat 20 --delay 30 1
fi
