# Scripts used with i3

Note on dependencies: most of them rely on various common Unix utilities (sed, cat, and grep) as well on the notification daemon dunst. If a certain script requires more, it will be mentioned below.

## `autoclick.sh`

Simulates a burst of mouse clicks, each one repeated after a 30 ms long delay.

Dependency: xdotool

## `blugon.sh`

`blugon.sh` allows one to map keyboard shortcuts to commands that reduces/raises the screen colour temperature.

Example i3 config:

```
set $mode_blugon blugon: (F1-F2) decrease/increase temp, (F3) manual mode, (F4) automatic mode, (F5) set temp to 6500
mode "$mode_blugon" {
	set $blugon ~/.config/i3/scripts/blugon
	bindsym F1 exec --no-startup-id $blugon decrease
	bindsym F2 exec --no-startup-id $blugon increase
	bindsym F3 exec --no-startup-id $blugon manual, mode "default"
	bindsym F4 exec --no-startup-id $blugon auto, mode "default"
	bindsym F5 exec --no-startup-id $blugon daytemp, mode "default"

	# back to normal: Enter or Escape
	bindsym Return mode "default"
	bindsym Escape mode "default"
}
# Enter blugon mode
bindsym $mod+r mode "$mode_blugon"
```

Dependencies:

- blugon
- systemd

## `blugon-launch.sh`

Used as a startup script. It manual mode was used before shutdown/reboot, switch to automatic mode.

Dependencies:

- blugon
- systemd

## `compton.sh`

Script for easily switching compton on and off. (It also includes a workaround for a visual glitch I have.)

Dependencies:

- compton
- xdotool (for the workaround)

## `idokep-*.sh`

These scripts parse idokep.hu (a Hungarian website dedicated to weather forecast). Currently, there are two such scripts available, one of which shows an animated radar map, while the other one shows brief information about any upcoming changes in barometric pressure.

Dependencies:

- mpv
- curl

## `redshift-x-brightness.sh`

Dependencies:

- zsh (for handling fractions)
- redshift

## `show-window-properties.sh`

It shows the title, class, and instance name of the window currently in focus.
