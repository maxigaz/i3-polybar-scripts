#!/usr/bin/env bash

if grep "readcurrent = True" ~/.config/blugon/config; then
	# Manual mode active. Switch to automatic mode
	sed -i -e 's|readcurrent = True|readcurrent = False|' ~/.config/blugon/config
	dunstify -r 12 -t 3000 "blugon" "automatic mode"
fi

systemctl --user restart blugon.service
