#!/bin/zsh
function get_current() {
	if [[ ! -f /tmp/redshift-current-brightness ]]; then
		echo "0.80" > /tmp/redshift-current-brightness
	fi
	cat /tmp/redshift-current-brightness
}

case "$1" in
	decrease)
		DECREASED=$(($(get_current)-0.10))
		echo "$DECREASED" > /tmp/redshift-current-brightness
		pkill -x redshift
		redshift -P -O 6500K -b $DECREASED:$DECREASED
		notify-send.sh --replace-file=/tmp/redshift-notification -t 3000 "Redshift" "Brightness: $(echo $DECREASED)"
		;;
	increase)
		INCREASED=$(($(get_current)+0.10))
		echo "$INCREASED" > /tmp/redshift-current-brightness
		pkill -x redshift
		redshift -P -O 6500K -b $INCREASED:$INCREASED
		notify-send.sh --replace-file=/tmp/redshift-notification -t 3000 "Redshift" "Brightness: $(echo $INCREASED)"
		;;
	reset)
		redshift -x
		redshift &!
		echo "0.80" > /tmp/redshift-current-brightness
		notify-send.sh --replace-file=/tmp/redshift-notification -t 3000 "Redshift" "Brightness reset"
		;;
	quit)
		pkill -x redshift
		redshift -x
		echo "1.00" > /tmp/redshift-current-brightness
		notify-send.sh --replace-file=/tmp/redshift-notification -t 3000 "Redshift" "Not running"
		;;
	*)
		echo "Usage: $0 {decrease|increase|reset|quit}"
		exit 2
esac

exit 0
