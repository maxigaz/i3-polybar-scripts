#!/bin/sh
decrease() {
blugon
}

case "$1" in
	decrease)
		blugon --setcurrent="-500"
		dunstify -r 12 -t 3000 "blugon" "$(cat ~/.config/blugon/current) K"
		;;
	increase)
		blugon --setcurrent="+500"
		dunstify -r 12 -t 3000 "blugon" "$(cat ~/.config/blugon/current) K"
		;;
	manual)
		sed -i -e 's|readcurrent = False|readcurrent = True|' ~/.config/blugon/config
		systemctl --user restart blugon.service
		dunstify -r 12 -t 3000 "blugon" "manual mode"
		;;
	auto)
		sed -i -e 's|readcurrent = True|readcurrent = False|' ~/.config/blugon/config
		systemctl --user restart blugon.service
		dunstify -r 12 -t 3000 "blugon" "automatic mode"
		;;
	daytemp)
		sed -i -e 's|readcurrent = False|readcurrent = True|' ~/.config/blugon/config
		blugon --setcurrent="6500"
		systemctl --user restart blugon.service
		dunstify -r 12 -t 3000 "blugon" "$(cat ~/.config/blugon/current) K"
		;;
	*)
		echo "Usage: $0 {decrease|increase|manual|auto|daytemp}"
		exit 2
esac

			exit 0
