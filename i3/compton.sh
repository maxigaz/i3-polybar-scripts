#!/usr/bin/env bash
if [[ $(pgrep -u $UID -x compton) ]]; then
	pkill compton
	dunstify -r 9 "Compton" "Shutdown"
else
	compton &!
	dunstify -r 9 "Compton" "(Re)started"
	# The following hides and unhides the tray area. Workaround for visual bug.
	xdotool windowunmap `xdotool search --name Polybar|head -1`
	xdotool windowmap `xdotool search --name Polybar|head -1`
fi
