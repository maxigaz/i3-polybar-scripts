#!/usr/bin/env bash
# Get the properties of the window currently in focus, then show them with dunst

DATA=`xprop -id $(xdotool getwindowfocus)`
TITLE=`grep "WM_NAME(STRING)" <<< $DATA | awk '{ $1=""; $2=""; print}' | sed -e "s|  \"||" -e "s|\"||"`
CLASS=`grep "WM_CLASS(STRING)" <<< $DATA | awk '{ print $4 }' | tr -d \",`
INSTANCE=`grep "WM_CLASS(STRING)" <<< $DATA | awk '{ print $3 }' | tr -d \",`

dunstify "$TITLE" "Class: $CLASS<br>Instance: $INSTANCE"
