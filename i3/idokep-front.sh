#!/usr/bin/env bash

DATA=$(curl -s https://www.idokep.hu/idojaras/Debrecen | grep -A 33 "shortCurrentWeatherText")
SUMMARY=$(sed '34q;d' <<< $DATA | sed 's/\s\s*//' | sed 's/<\/div>//')

dunstify -t 2000 --icon=weather "Időkép: orvosmeteorológia" "$SUMMARY"
